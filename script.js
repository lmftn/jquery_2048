(function($)
{
  $.fn.pluginlaunch = function() {
    return this.each(function()
    {
      main();
    });

    function main(){
    audio = new Audio('whatlove.mp3');
    audio.play();
    document.querySelector('#play').onclick = function() {
      audio.play();
    }
    document.querySelector('#pause').onclick = function() {
      audio.pause();
    }
    document.querySelector('#mute').onclick = function() {
      if(audio.muted == false) {
        audio.muted = true;
      }
      else {
        audio.muted = false;
      }
    }
      $(".start").click(function(){
        newgame();
      });

      newgame();
     if($('#overlay').css('visibility') == 'hidden'){

      $("html").keyup(function(event){
        if(event.which == 38) { // key up
          addmade = false;
          movemade = false;
          goup();
          if(addmade == true || movemade == true) {
            addcell();
          }
          var plein = plain();
          var poss = possadd();
          if(plain() == false && possadd() == false) {
            $("#overlay").empty();
            $("#overlay").append("<p id='overlaygo'>GAME OVER</p><p id='overlaysco'>Your score was <p id='finalsco'>" + score + "</p></p>");
            overlay();
          }
        }
        else if(event.which == 40) { // key down
          addmade = false;
          movemade = false;
          godown();
          if(addmade == true || movemade == true) {
            addcell();
          }
          var plein = plain();
          var poss = possadd();
          if(plain() == false && possadd() == false) {
            $("#overlay").empty();
            $("#overlay").append("<p id='overlaygo'>GAME OVER</p><p id='overlaysco'>Your score was <p id='finalsco'>" + score + "</p></p>");
            overlay();
          }
        }
        else if(event.which == 39) { // key right
          addmade = false;
          movemade = false;
          goright();
          if(addmade == true || movemade == true) {
            addcell();
          }
          var plein = plain();
          var poss = possadd();
          if(plain() == false && possadd() == false) {
            $("#overlay").empty();
            $("#overlay").append("<p id='overlaygo'>GAME OVER</p><p id='overlaysco'>Your score was <p id='finalsco'>" + score + "</p></p>");
            overlay();
          }
        }
        else if(event.which == 37) { // key left
          addmade = false;
          movemade = false;
          goleft();
          if(addmade == true || movemade == true) {
            addcell();
          }
          var plein = plain();
          var poss = possadd();
          if(plain() == false && possadd() == false) {
            $("#overlay").empty();
            $("#overlay").append("<p id='overlaygo'>GAME OVER</p><p id='overlaysco'>Your score was <p id='finalsco'>" + score + "</p></p>");
            overlay();
          }
        }
      });
    }
    }

    function newgame(){
      $('#overlay').css("visibility", "hidden");
      $('#overlay').attr("z-index", "-1");
      score = 0;
      var col = Math.floor(Math.random() * (5 - 1) + 1);
      var row = Math.floor(Math.random() * (5 - 1) + 1);
      var alea = Math.floor(Math.random() * 50);
      var nb = 2;
      var nr = 1;
      while(nr <= 4){
        var nc = 1;
        while(nc <= 4){
          $(".grid-row:nth-child(" + nr + ") .grid-cell:nth-child(" + nc + ")").empty();
          $("#score").empty();
          $(".grid-row:nth-child(" + nr + ") .grid-cell:nth-child(" + nc + ")").removeClass("quatre");
          $(".grid-row:nth-child(" + nr + ") .grid-cell:nth-child(" + nc + ")").removeClass("huit");
          $(".grid-row:nth-child(" + nr + ") .grid-cell:nth-child(" + nc + ")").removeClass("seize");
          $(".grid-row:nth-child(" + nr + ") .grid-cell:nth-child(" + nc + ")").removeClass("trente");
          $(".grid-row:nth-child(" + nr + ") .grid-cell:nth-child(" + nc + ")").removeClass("soixante");
          $(".grid-row:nth-child(" + nr + ") .grid-cell:nth-child(" + nc + ")").removeClass("cent");
          $(".grid-row:nth-child(" + nr + ") .grid-cell:nth-child(" + nc + ")").removeClass("deuxcent");
          $(".grid-row:nth-child(" + nr + ") .grid-cell:nth-child(" + nc + ")").removeClass("cinqcent");
          $(".grid-row:nth-child(" + nr + ") .grid-cell:nth-child(" + nc + ")").removeClass("mille");
          $(".grid-row:nth-child(" + nr + ") .grid-cell:nth-child(" + nc + ")").removeClass("deuxmille");
          nc++;
        }
        nr++;
      }
      $(".grid-row:nth-child(" + col + ") .grid-cell:nth-child(" + row + ")").append(nb);
      if(alea == 18) {
        nb = 4;
      }
      if(row == col && row == 4) {
        row -= 1;
      }
      else if(row == col){
        row += 1;
      }
      if(nb == 4) {
        $(".grid-row:nth-child(" + row + ") .grid-cell:nth-child(" + col + ")").addClass("quatre");
        $(".grid-row:nth-child(" + row + ") .grid-cell:nth-child(" + col + ")").append(nb);
      }
      else {
        $(".grid-row:nth-child(" + row + ") .grid-cell:nth-child(" + col + ")").append(nb);
      }
    }

    function plain() {
      var plain = false;
      var nr = 1;
      while(nr <= 4){
        var nc = 1;
        while(nc <= 4) {
          if($(".grid-row:nth-child(" + nr + ") .grid-cell:nth-child(" + nc + ")").text() == "") {
            var plain = true;
            return plain;
          }
          nc++;
        }
        nr++;
      }
      return plain;
    }

    function possadd() {
      var poss = false;
      var nc = 1;
      var nc1 = 2;
      var nr = 1;
      while(nr <= 4) {
        nc = 1;
        nc1 = 2;
        while(nc1 <= 4) {
          if($(".grid-row:nth-child(" + nr + ") .grid-cell:nth-child(" + nc + ")").text() == $(".grid-row:nth-child(" + nr + ") .grid-cell:nth-child(" + nc1 + ")").text()) {
            var poss = true;
            return poss;
          }
          nc++;
          nc1++;
        }
        nr++;
      }
      var nc = 1;
      while(nc <= 4) {
        nr = 1;
        nr1 = 2;
        while(nr1 <= 4) {
          if($(".grid-row:nth-child(" + nr + ") .grid-cell:nth-child(" + nc + ")").text() == $(".grid-row:nth-child(" + nr1 + ") .grid-cell:nth-child(" + nc + ")").text()) {
            var poss = true;
            return poss;
          }
          nr++;
          nr1++;
        }
        nc++;
      }
      return poss;
    }

    function overlay() {

          $('#overlay').css("visibility", "visible");
          $('#overlay').attr("z-index", "2");
  }

    function addition(itemdp, item1, item2){
      var nb1 = item1.text();
      var nb2 = item2.text();
      var nb3 = parseInt(nb1) + parseInt(nb2);
      item1.empty();
      item1.removeClass();
      item1.addClass("grid-cell");
      item2.empty();
      item2.removeClass();
      item2.addClass("grid-cell");
      itemdp.empty();
      itemdp.removeClass();
      itemdp.addClass("grid-cell");
      itemdp.append(nb3);
      clss(itemdp, nb3);
      score = parseInt(score) + parseInt(nb3);
      $("#score").empty();
      $("#score").append(score);
      if(nb3 == 2048){
        $("#overlay").empty();
        $("#overlay").append("<p id='overlaygo'>YOU WON !</p><p id='overlaysco'>Your score was " + score + "</p>");
        overlay();
    }
      addmade = true;
    }

    function move(item1, itemdp){
      var nb = item1.text();
      item1.empty();
      item1.removeClass();
      item1.addClass("grid-cell");
      itemdp.empty();
      item1.removeClass();
      item1.addClass("grid-cell");
      itemdp.append(nb);
      clss(itemdp, nb);
      movemade = true;
    }

    function clss(itemdp, nb) {
      if(nb == 4) {
        itemdp.addClass("quatre");
      }
      if(nb == 8) {
        itemdp.addClass("huit");
      }
      if(nb == 16) {
        itemdp.addClass("seize");
      }
      if(nb == 32) {
        itemdp.addClass("trente");
      }
      if(nb == 64) {
        itemdp.addClass("soixante");
      }
      if(nb == 128) {
        itemdp.addClass("cent");
      }
      if(nb == 256) {
        itemdp.addClass("deuxcent");
      }
      if(nb == 512) {
        itemdp.addClass("cinqcent");
      }
      if(nb == 1024) {
        itemdp.addClass("mille");
      }
      if(nb == 2048) {
        itemdp.addClass("deuxmille");
      }
    }

    function addcell() {
      var col = Math.floor(Math.random() * (5 - 1) + 1);
      var row = Math.floor(Math.random() * (5 - 1) + 1);
      var alea = Math.floor(Math.random() * 50);
      var nb = 2;
      if(alea == 18) {
        nb = 4;
      }
      if($(".grid-row:nth-child(" + row + ") .grid-cell:nth-child(" + col + ")").text() == "") {
        if(nb == 4) {
          $(".grid-row:nth-child(" + row + ") .grid-cell:nth-child(" + col + ")").addClass("quatre");
        }
        else {
          $(".grid-row:nth-child(" + row + ") .grid-cell:nth-child(" + col + ")").append(nb);
        }
      }
      else {
        addcell();
      }
    }

    function goup(){
      var nb = 1;
      while(nb <= 4) {
        var item1 = $('.grid-row:nth-child(1) .grid-cell:nth-child(' + nb + ')');
        var item2 = $('.grid-row:nth-child(2) .grid-cell:nth-child(' + nb + ')');
        var item3 = $('.grid-row:nth-child(3) .grid-cell:nth-child(' + nb + ')');
        var item4 = $('.grid-row:nth-child(4) .grid-cell:nth-child(' + nb + ')');
        if(item1.text() != "") {
          if(item1.text() == item2.text()){
            addition(item1, item1, item2);
          }
          else if(item1.text() == item3.text() && item2.text() == ""){
            addition(item1, item1, item3);
          }
          else if(item1.text() == item4.text() && item2.text() == "" && item3.text() == ""){
            addition(item1, item1, item4);
          }
        }
        if(item2.text() != ""){
          if(item2.text() == item3.text()){
            if(item1.text() == ""){
              addition(item1, item2, item3);
            }
            else {
              addition(item2, item2, item3);
            }
          }
          else if(item2.text() == item4.text() && item3.text() == ""){
            if(item1.text() == ""){
              addition(item1, item2, item4);
            }
            else {
              addition(item2, item2, item4);
            }
          }
        }
        if(item3.text() != ""){
          if(item3.text() == item4.text()) {
            if(item1.text() == "" && item2.text() == ""){
              addition(item1, item3, item4);
            }
            else if(item2.text() == ""){
              addition(item2, item3, item4);
            }
            else {
              addition(item3, item3, item4);
            }
          }
        }
        if(item2.text() != "" && item1.text() == "") {
          move(item2, item1);
        }
        if(item3.text() != "" && item1.text() == "" && item2.text() == "") {
          move(item3, item1);
        }
        else if(item3.text() != "" && item2.text() == "") {
          move(item3, item2);
        }
        if(item4.text() != "" && item1.text() == "" && item2.text() == "" && item3.text() == "") {
          move(item4, item1);
        }
        else if(item4.text() != "" && item2.text() == "" && item3.text() == "") {
          move(item4, item2);
        }
        else if(item4.text() != "" && item3.text() == ""){
          move(item4, item3);
        }
        nb++;
      }
    }

    function godown(){
      var nb = 1;
      while(nb <= 4) {
        var item1 = $('.grid-row:nth-child(4) .grid-cell:nth-child(' + nb + ')');
        var item2 = $('.grid-row:nth-child(3) .grid-cell:nth-child(' + nb + ')');
        var item3 = $('.grid-row:nth-child(2) .grid-cell:nth-child(' + nb + ')');
        var item4 = $('.grid-row:nth-child(1) .grid-cell:nth-child(' + nb + ')');
        if(item1.text() != "") {
          if(item1.text() == item2.text()){
            addition(item1, item1, item2);
          }
          else if(item1.text() == item3.text() && item2.text() == ""){
            addition(item1, item1, item3);
          }
          else if(item1.text() == item4.text() && item2.text() == "" && item3.text() == ""){
            addition(item1, item1, item4);
          }
        }
        if(item2.text() != ""){
          if(item2.text() == item3.text()){
            if(item1.text() == ""){
              addition(item1, item2, item3);
            }
            else {
              addition(item2, item2, item3);
            }
          }
          else if(item2.text() == item4.text() && item3.text() == ""){
            if(item1.text() == ""){
              addition(item1, item2, item4);
            }
            else {
              addition(item2, item2, item4);
            }
          }
        }
        if(item3.text() != ""){
          if(item3.text() == item4.text()) {
            if(item1.text() == "" && item2.text() == ""){
              addition(item1, item3, item4);
            }
            else if(item2.text() == ""){
              addition(item2, item3, item4);
            }
            else {
              addition(item3, item3, item4);
            }
          }
        }
        if(item2.text() != "" && item1.text() == "") {
          move(item2, item1);
        }
        if(item3.text() != "" && item1.text() == "" && item2.text() == "") {
          move(item3, item1);
        }
        else if(item3.text() != "" && item2.text() == "") {
          move(item3, item2);
        }
        if(item4.text() != "" && item1.text() == "" && item2.text() == "" && item3.text() == "") {
          move(item4, item1);
        }
        else if(item4.text() != "" && item2.text() == "" && item3.text() == "") {
          move(item4, item2);
        }
        else if(item4.text() != "" && item3.text() == ""){
          move(item4, item3);
        }
        nb++;
      }
    }

    function goleft(){
      var nb = 1;
      while(nb <= 4) {
        var item1 = $('.grid-row:nth-child(' + nb + ') .grid-cell:nth-child(1)');
        var item2 = $('.grid-row:nth-child(' + nb + ') .grid-cell:nth-child(2)');
        var item3 = $('.grid-row:nth-child(' + nb + ') .grid-cell:nth-child(3)');
        var item4 = $('.grid-row:nth-child(' + nb + ') .grid-cell:nth-child(4)');
        if(item1.text() != "") {
          if(item1.text() == item2.text()){
            addition(item1, item1, item2);
          }
          else if(item1.text() == item3.text() && item2.text() == ""){
            addition(item1, item1, item3);
          }
          else if(item1.text() == item4.text() && item2.text() == "" && item3.text() == ""){
            addition(item1, item1, item4);
          }
        }
        if(item2.text() != ""){
          if(item2.text() == item3.text()){
            if(item1.text() == ""){
              addition(item1, item2, item3);
            }
            else {
              addition(item2, item2, item3);
            }
          }
          else if(item2.text() == item4.text() && item3.text() == ""){
            if(item1.text() == ""){
              addition(item1, item2, item4);
            }
            else {
              addition(item2, item2, item4);
            }
          }
        }
        if(item3.text() != ""){
          if(item3.text() == item4.text()) {
            if(item1.text() == "" && item2.text() == ""){
              addition(item1, item3, item4);
            }
            else if(item2.text() == ""){
              addition(item2, item3, item4);
            }
            else {
              addition(item3, item3, item4);
            }
          }
        }
        if(item2.text() != "" && item1.text() == "") {
          move(item2, item1);
        }
        if(item3.text() != "" && item1.text() == "" && item2.text() == "") {
          move(item3, item1);
        }
        else if(item3.text() != "" && item2.text() == "") {
          move(item3, item2);
        }
        if(item4.text() != "" && item1.text() == "" && item2.text() == "" && item3.text() == "") {
          move(item4, item1);
        }
        else if(item4.text() != "" && item2.text() == "" && item3.text() == "") {
          move(item4, item2);
        }
        else if(item4.text() != "" && item3.text() == ""){
          move(item4, item3);
        }
        nb++;
      }
    }

    function goright(){
      var nb = 1;
      while(nb <= 4) {
        var item4 = $('.grid-row:nth-child(' + nb + ') .grid-cell:nth-child(1)');
        var item3 = $('.grid-row:nth-child(' + nb + ') .grid-cell:nth-child(2)');
        var item2 = $('.grid-row:nth-child(' + nb + ') .grid-cell:nth-child(3)');
        var item1 = $('.grid-row:nth-child(' + nb + ') .grid-cell:nth-child(4)');
        if(item1.text() != "") {
          if(item1.text() == item2.text()){
            addition(item1, item1, item2);
          }
          else if(item1.text() == item3.text() && item2.text() == ""){
            addition(item1, item1, item3);
          }
          else if(item1.text() == item4.text() && item2.text() == "" && item3.text() == ""){
            addition(item1, item1, item4);
          }
        }
        if(item2.text() != ""){
          if(item2.text() == item3.text()){
            if(item1.text() == ""){
              addition(item1, item2, item3);
            }
            else {
              addition(item2, item2, item3);
            }
          }
          else if(item2.text() == item4.text() && item3.text() == ""){
            if(item1.text() == ""){
              addition(item1, item2, item4);
            }
            else {
              addition(item2, item2, item4);
            }
          }
        }
        if(item3.text() != ""){
          if(item3.text() == item4.text()) {
            if(item1.text() == "" && item2.text() == ""){
              addition(item1, item3, item4);
            }
            else if(item2.text() == ""){
              addition(item2, item3, item4);
            }
            else {
              addition(item3, item3, item4);
            }
          }
        }
        if(item2.text() != "" && item1.text() == "") {
          move(item2, item1);
        }
        if(item3.text() != "" && item1.text() == "" && item2.text() == "") {
          move(item3, item1);
        }
        else if(item3.text() != "" && item2.text() == "") {
          move(item3, item2);
        }
        if(item4.text() != "" && item1.text() == "" && item2.text() == "" && item3.text() == "") {
          move(item4, item1);
        }
        else if(item4.text() != "" && item2.text() == "" && item3.text() == "") {
          move(item4, item2);
        }
        else if(item4.text() != "" && item3.text() == ""){
          move(item4, item3);
        }
        nb++;
      }
    }
  };
})(jQuery);
